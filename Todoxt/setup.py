# -*- coding: utf-8 -*-

import sys
from cx_Freeze import setup, Executable

base = None
if sys.platform == 'win32':
    base = 'Win32GUI'



data_files = [('',['view-refresh.png','preferences-system.png','icon.png'])]

shortcut_table = [
    ("DesktopShortcut",        # Shortcut
     "DesktopFolder",          # Directory_
     "Todoxt",           # Name
     "TARGETDIR",              # Component_
     "[TARGETDIR]\Todoxt.exe",# Target
     None,                     # Arguments
     None,                     # Description
     None,                     # Hotkey
     None,                     # Icon
     None,                     # IconIndex
     None,                     # ShowCmd
     "TARGETDIR",               # WkDir
     )
    ]

# Now create the table dictionary
msi_data = {"Shortcut": shortcut_table}

# Change some default MSI options and specify the use of the above defined tables
bdist_msi_options = {'data': msi_data,'upgrade_code':'{31C67847-EFDD-47F5-9818-7CE5505042CC}'}


##executables = [
##    Executable('Todoxt.py', base=base)
##]

executables = [Executable(
    "Todoxt.py",
    base="Win32GUI",
    icon="icon.ico"
    )]

options = {
    'build_exe': {
        'includes': 'atexit'
    },"bdist_msi": bdist_msi_options
}

setup(name='Todoxt',
      shortcutName="Todoxt",
      shortcutDir="DesktopFolder",
      version='0.13',
      description='Todoxt GUI application',
      options=options,
      executables=executables,
      data_files = data_files
)
