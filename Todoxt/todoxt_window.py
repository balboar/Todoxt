# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'main_windows.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import QWaitCondition


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 626)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")

        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.verticalLayout_2.setContentsMargins(0, 6, 0, 0)
        self.verticalLayout_2.setSpacing(6)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.horizontalLayout.setContentsMargins(6, 0, 6, 0)
        self.horizontalLayout.setSpacing(6)
        # self.label = QtWidgets.QLabel(self.centralwidget)
        # self.label.setObjectName("label")
        # self.horizontalLayout.addWidget(self.label)
        self.findEdit = QtWidgets.QLineEdit(self.centralwidget)
        self.findEdit.setObjectName("findEdit")
        self.findEdit.setMaximumWidth(200)
        self.horizontalLayout.addWidget(self.findEdit)
        self.EditNew = QtWidgets.QLineEdit(self.centralwidget)
        self.EditNew.setObjectName("EditNew")
        self.EditNew.setPlaceholderText('Add a new task')
        # self.verticalLayout.addWidget(self.EditNew)
        self.horizontalLayout.addWidget(self.EditNew)

        self.btnRefresh = QtWidgets.QToolButton(self.centralwidget)
        icon=QtGui.QIcon("view-refresh.png")
        self.btnRefresh.setIcon(icon)
        self.btnRefresh.setObjectName("btnRefresh")
        self.horizontalLayout.addWidget(self.btnRefresh)
        self.btnSettings = QtWidgets.QToolButton(self.centralwidget)
        icon = QtGui.QIcon("preferences-system.png")
        self.btnSettings.setIcon(icon)
        self.btnSettings.setObjectName("btnSettings")
        self.horizontalLayout.addWidget(self.btnSettings)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.treeWidget = QtWidgets.QTreeWidget(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        self.treeWidget.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        sizePolicy.setHeightForWidth(self.treeWidget.sizePolicy().hasHeightForWidth())
        self.treeWidget.setSizePolicy(sizePolicy)
        self.treeWidget.setMouseTracking(True)
        self.treeWidget.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.treeWidget.setSizeAdjustPolicy(QtWidgets.QAbstractScrollArea.AdjustToContentsOnFirstShow)
        self.treeWidget.setProperty("showDropIndicator", False)
        self.treeWidget.setAlternatingRowColors(False)
        self.treeWidget.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.treeWidget.setRootIsDecorated(False)
        self.treeWidget.setUniformRowHeights(False)
        self.treeWidget.setItemsExpandable(False)
        self.treeWidget.setColumnCount(6)
        self.treeWidget.setObjectName("treeWidget")
        self.treeWidget.frameShape = QtWidgets.QFrame.NoFrame
        # self.treeWidget.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        # item_0 = QtWidgets.QTreeWidgetItem(self.treeWidget)
        # item_0.setCheckState(0, QtCore.Qt.Checked)
        self.treeWidget.header().setVisible(True)
        self.treeWidget.setAlternatingRowColors(True)
        self.treeWidget.header().setStretchLastSection(False)
        self.treeWidget.header().setSectionResizeMode(QtWidgets.QHeaderView.Interactive)
        self.verticalLayout.addWidget(self.treeWidget)
        # self.EditNew = QtWidgets.QLineEdit(self.centralwidget)

        # self.statusbar = QtWidgets.QStatusBar()
        # self.verticalLayout.addWidget(self.statusbar)
        self.verticalLayout_2.addLayout(self.verticalLayout)
        MainWindow.setCentralWidget(self.centralwidget)
        self.actionConfiguracion = QtWidgets.QAction(MainWindow)
        self.actionConfiguracion.setCheckable(True)
        self.treeWidget.setColumnWidth(0,18)
        self.treeWidget.setColumnWidth(1,380)
        # self.treeWidget.setColumnWidth(5, 100)
        self.treeWidget.sortByColumn(1,QtCore.Qt.AscendingOrder)
        self.actionConfiguracion.setObjectName("actionConfiguracion")
        self.findEdit.setClearButtonEnabled(True)
        self.findEdit.setPlaceholderText('Search')
        icon = QtGui.QIcon("icon.png")
        MainWindow.setWindowIcon(icon)
        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)



    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Todoxt"))
        # self.label.setText(_translate("MainWindow", "Find"))
        self.btnRefresh.setText(_translate("MainWindow", "..."))
        self.btnRefresh.setShortcut(_translate("MainWindow", "Ctrl+R"))
        self.btnSettings.setText(_translate("MainWindow", "..."))
        self.btnSettings.setShortcut(_translate("MainWindow", "Ctrl+N"))
        self.treeWidget.setSortingEnabled(True)
        self.treeWidget.headerItem().setText(0, _translate("MainWindow", ""))
        self.treeWidget.headerItem().setText(1, _translate("MainWindow", "Todo"))
        self.treeWidget.headerItem().setText(2, _translate("MainWindow", "Creation date"))
        self.treeWidget.headerItem().setText(3, _translate("MainWindow", "Due"))
        self.treeWidget.headerItem().setText(4, _translate("MainWindow", "Context"))
        self.treeWidget.headerItem().setText(5, _translate("MainWindow", "Project"))
        self.actionConfiguracion.setText(_translate("MainWindow", "Configuration"))
        self.treeWidget.setAlternatingRowColors(False)



class customQtTreeWidgetItem(QtWidgets.QTreeWidgetItem):

    def __init__(self, todo):
        super(customQtTreeWidgetItem, self).__init__()
        self._todo = todo









import resource
